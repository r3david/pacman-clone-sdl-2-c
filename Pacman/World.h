//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef WORLD_H
#define WORLD_H

#include "Vector2f.h"
#include "Coord2i.h"
#include "Drawer.h"
#include "ResourcesManager.h"
#include "PathmapTile.h"
#include <vector>

class World
{
public:
	World();
	~World();

	bool Init(ResourcesManager* aResourceManager, ItemProvider* aItemProvider);
	void DeInit();
	void Draw(Drawer* aDrawer);

	// queries
	bool IsTileWalkable(int aTileX, int aTileY) const;
	PathmapTile* GetTile(int aTileX, int aTileY) const;
	bool AreCoordsOutOfBounds(int aTileX, int aTileY) const;
	std::vector<PathmapTile*>& GetTilesArray() { return myPathmapTiles; }
	size_t GetNumberOfDotsInWorld() { return myNumberOfDots; } 

	// coordinates handling
	size_t GetMapTileSize() const { return myMapTileSize; }
	size_t GetMapSizeX() const { return myMapSizeX; }
	size_t GetMapSizeY() const { return myMapSizeY; }
	Coord2i ConvertWorldToTileCoords(Vector2f aWorldPos);
	Vector2f ConvertTileToWorldCoords(Coord2i aTilePos);
	size_t GetTileIndexForTileCoordinates(int aTileX, int aTileY) const;
	Coord2i GetTileCoordsFromTileIndex(size_t index) const;

private:
	bool LoadMap(ItemProvider* aItemProvider);

	size_t myMapTileSize = 22;
	float myMapTileSizeInv = 1.f / 22.f;
	size_t myMapSizeX;//number of tiles in the X axis
	size_t myMapSizeY; //number of tiles in the Y axis
	size_t myNumberOfDots;
	TextureAsset* myMapBackground;
	std::vector<PathmapTile*> myPathmapTiles;
};

#endif // WORLD_H
//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef PATHMAPTILE_H
#define PATHMAPTILE_H

#include "Drawer.h"
#include "ItemProvider.h"

class PathmapTile
{
public:
	PathmapTile(int aTileX, int aTileY, int aTileSize);
	~PathmapTile(void);

	size_t Init(ItemProvider* aItemProvider, const char aTileChar);
	void Draw(Drawer* aDrawer) const;
	void OnItemPickedUp(ItemProvider* aItemProvider);

	int myX; // (0, nTiles in X)
	int myY; // (0, nTiles in Y)
	bool myIsBlockingFlag;
	bool myIsVisitedFlag;

	Item* GetTileItem() { return myTileItem; }

private:
	int myWorldX;
	int myWorldY;
	Item* myTileItem; 
};

#endif // PATHMAPTILE_H
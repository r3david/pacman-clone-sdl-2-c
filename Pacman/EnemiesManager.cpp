//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "EnemiesManager.h"

EnemiesManager::EnemiesManager()
{
}

EnemiesManager::~EnemiesManager()
{
}

bool EnemiesManager::Init(EnemiesDef* aDef, World* aWorld, ResourcesManager* aResourcesManager)
{
	// instantiate all ghosts
	for(size_t i = 0; i < 4; ++i)
	{
		Ghost* newGhost = new Ghost(static_cast<GhostType>(i));
		if (!newGhost->Init(aDef, aWorld, aResourcesManager))
			return false;

		myGhosts.push_back(newGhost);
	}

	return true;
}

void EnemiesManager::AtGameStarts(World * aWorld)
{
	for(Ghost*& ghost : myGhosts)
		ghost->AtGameStarts(aWorld);
}

void EnemiesManager::OnGhostsBecomeVulnerable(World * aWorld)
{
	for (Ghost*& ghost : myGhosts)
		ghost->OnVulnerable(aWorld);
}

void EnemiesManager::DeInit()
{
	for (Ghost*& ghost : myGhosts)
	{
		ghost->DeInit();
		delete ghost;
	}
}

void EnemiesManager::Update(GameTime* aTime, Pacman* aPlayer, World* aWorld)
{
	for(Ghost*& ghost : myGhosts)
		ghost->Update(aTime, aPlayer, aWorld);
}

void EnemiesManager::Draw(Drawer * aDrawer)
{
	for (Ghost*& ghost : myGhosts)
		ghost->Draw(aDrawer);
}


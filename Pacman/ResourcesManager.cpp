//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "ResourcesManager.h"

ResourcesManager::ResourcesManager()
{
}

ResourcesManager::~ResourcesManager()
{
}

bool ResourcesManager::Init()
{
	myTextureResourcesDB = new ResourceDatabase<TextureAsset>();
	mySpriteAnimationResourcesDB = new ResourceDatabase<SpriteAnimation>();
	myFontResourcesDB = new ResourceDatabase<FontAsset>();

	return true;
}

void ResourcesManager::DeInit()
{
	mySpriteAnimationResourcesDB->DeInit();
	myTextureResourcesDB->DeInit();
	myFontResourcesDB->DeInit();

	delete mySpriteAnimationResourcesDB;
	delete myTextureResourcesDB;
	delete myFontResourcesDB;
}

void* ResourcesManager::GetResource(const char* aResourceName, ResourceType aRType)
{
	switch(aRType)
	{
		case Texture : return myTextureResourcesDB->GetResource(aResourceName);
		case Animation : return mySpriteAnimationResourcesDB->GetResource(aResourceName);
		case Font : return myFontResourcesDB->GetResource(aResourceName);
	}

	return nullptr;
}

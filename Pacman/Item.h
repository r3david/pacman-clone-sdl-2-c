//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef ITEM_H
#define ITEM_H

#include "TextureAsset.h"

class Drawer;
class PathmapTile;

// this class represents an Item for the pacman game
// observe it doesnt have a position. The idea is to have only one single instance of a type of item and reuse at any tile. 
// in this way we have 4 instances of items. Each tile will have a reference to any of this 4 instances. This can be seen as the Flyweight Programming Pattern.
// also observe that the null type can be used for tiles that doesnt have any items on them.
class Item
{
public:
	enum ItemType : unsigned int
	{
		NullItem = 0,
		Dot,
		BigDot,
		Cherry
	};
	
	// unwanted CTORS
	Item() = delete;
	// TODO other CTORS delete here...

	// Interface
	virtual ~Item();
	virtual void OnPickedUp(PathmapTile* aTilePick) const = 0; 
	TextureAsset* GetTextureAsset() const { return myTexture; }

protected:
	Item(ItemType aType, TextureAsset* aTextureAsset);

	ItemType myType;
	TextureAsset* myTexture;
};

#endif // ITEM_H
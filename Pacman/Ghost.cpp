//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "Ghost.h"
#include "World.h"
#include "PathmapTile.h"
#include "Drawer.h"
#include "Game.h"

Ghost::Ghost(GhostType aType) :
	GameEntity(Vector2f(0.f, 0.f)),
	myGhostType(aType)
{
}

Ghost::~Ghost()
{
}

bool Ghost::Init(EnemiesDef* aDef, World* aWorld, ResourcesManager* aResourcesManager)
{
	if (aWorld == nullptr || aResourcesManager == nullptr)
		return false;

	switch(myGhostType)
	{
		case GhostType::Red :
		{
			myGhostTexture = (TextureAsset*)aResourcesManager->GetResource("ghost_32_red.png", ResourcesManager::Texture);
			myParams = &aDef->myRedGhostParams;
			break;
		}
		case GhostType::Cyan:
		{
			myGhostTexture = (TextureAsset*)aResourcesManager->GetResource("ghost_32_cyan.png", ResourcesManager::Texture);
			myParams = &aDef->myCyanGhostParams;
			break;
		}
		case GhostType::Orange: 
		{
			myGhostTexture = (TextureAsset*)aResourcesManager->GetResource("ghost_32_orange.png", ResourcesManager::Texture);
			myParams = &aDef->myOrangeGhostParams;
			break;
		}
		case GhostType::Pink:
		{
			myGhostTexture = (TextureAsset*)aResourcesManager->GetResource("ghost_32_pink.png", ResourcesManager::Texture);
			myParams = &aDef->myPinkGhostParams;
			break;
		}
	}

	// initialize components ( initialize with default ghost texture )
	myMotionComponent = new MotionComponent(this, aWorld);

	myGhostDeadTexture = (TextureAsset*)aResourcesManager->GetResource("Ghost_Dead_32.png", ResourcesManager::Texture);
	myGhostVulnerableTexture = (TextureAsset*)aResourcesManager->GetResource("Ghost_Vulnerable_32.png", ResourcesManager::Texture);

	if(myGhostTexture == nullptr)
		myGhostTexture = (TextureAsset*)aResourcesManager->GetResource("ghost_32.png", ResourcesManager::Texture);

	myStaticTextureRenderer = new StaticTextureRenderer(this, myGhostTexture);

	// initialize FSM
	if(myParams == nullptr)
		myParams = &aDef->myRedGhostParams; // use by default red params

	myGhostBehaviourFSM = new GhostBehaviourFSM();
	if(!myGhostBehaviourFSM->InitFSM(this))
		return false;

	return true;
}

void Ghost::DeInit()
{
	myGhostBehaviourFSM->DeInit();

	delete myGhostBehaviourFSM;
	delete myMotionComponent;
	delete myStaticTextureRenderer;	
}

void Ghost::Draw(Drawer * aDrawer)
{
	myStaticTextureRenderer->Render(aDrawer);
}

void Ghost::Update(GameTime* aTime, Pacman* aPlayer, World* aWorld)
{
	myGhostBehaviourFSM->Update(aTime, aPlayer, aWorld);
	myMotionComponent->Update(aTime);
}

void Ghost::AtGameStarts(World* aWorld)
{
	myMotionComponent->InitializeAtPosition(myParams->mySpawnPosition * static_cast<float>(aWorld->GetMapTileSize()), aWorld);

	if(myGhostType == GhostType::Red)
		myGhostBehaviourFSM->StartOnState(GhostBehaviourState::BehaviourType::Chase, aWorld);
	else
		myGhostBehaviourFSM->StartOnState(GhostBehaviourState::BehaviourType::AtRest, aWorld);
}

void Ghost::OnGhostKilled(World* aWorld)
{
	myStaticTextureRenderer->SetTextureAsset(myGhostDeadTexture);
	myGhostBehaviourFSM->SetToState(GhostBehaviourState::BehaviourType::Dead, aWorld);
}

void Ghost::OnVulnerable(World* aWorld)
{
	GhostBehaviourState::BehaviourType behType = myGhostBehaviourFSM->GetCurrentBehaviour();
	if (behType != GhostBehaviourState::BehaviourType::Dead && behType != GhostBehaviourState::BehaviourType::AtRest)
	{
		myStaticTextureRenderer->SetTextureAsset(myGhostVulnerableTexture);
		myGhostBehaviourFSM->SetToState(GhostBehaviourState::BehaviourType::Vulnerable, aWorld);
	}
}

void Ghost::OnVulnerableTimerFinishes(World* aWorld)
{
	myStaticTextureRenderer->SetTextureAsset(myGhostTexture);
	myGhostBehaviourFSM->ResumeBehaviourFromVulnerable(aWorld);
}

void Ghost::OnFinishedResting(World * aWorld)
{
	if (myGhostType == GhostType::Red || myGhostType == GhostType::Pink)
		myGhostBehaviourFSM->StartOnState(GhostBehaviourState::BehaviourType::Chase, aWorld);
	else
		myGhostBehaviourFSM->StartOnState(GhostBehaviourState::BehaviourType::Scatter, aWorld);
}

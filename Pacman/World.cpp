//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "World.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "PathmapTile.h"
#include "Dot.h"
#include "BigDot.h"
#include "Drawer.h"

World::World()
{
}

World::~World()
{
}

bool World::Init(ResourcesManager* aResourceManager, ItemProvider* aItemProvider)
{
	if (aResourceManager == nullptr || aItemProvider == nullptr)
		return false;

	myMapBackground = (TextureAsset*) aResourceManager->GetResource("playfield.png", ResourcesManager::Texture);
	return LoadMap(aItemProvider);
}

void World::DeInit()
{
}

void World::Draw(Drawer* aDrawer)
{
	aDrawer->Draw(myMapBackground);
	
	for(std::vector<PathmapTile*>::iterator iter = myPathmapTiles.begin(); iter != myPathmapTiles.end(); ++iter)
		(*iter)->Draw(aDrawer);
}

bool World::IsTileWalkable(int aTileX, int aTileY) const
{
	if (AreCoordsOutOfBounds(aTileX, aTileY))
		return false;
	
	PathmapTile* tile = myPathmapTiles[GetTileIndexForTileCoordinates(aTileX, aTileY)];
	return !tile->myIsBlockingFlag;
}

PathmapTile* World::GetTile(int aTileX, int aTileY) const
{
	if (AreCoordsOutOfBounds(aTileX, aTileY))
		return nullptr;

	return myPathmapTiles[GetTileIndexForTileCoordinates(aTileX, aTileY)];
}

bool World::AreCoordsOutOfBounds(int aTileX, int aTileY) const
{
	return aTileX < 0 || aTileX >= static_cast<int>(myMapSizeX) || aTileY < 0 || aTileY >= static_cast<int>(myMapSizeY);
}

Coord2i World::ConvertWorldToTileCoords(Vector2f aWorldPos)
{
	return{ (int) rint(aWorldPos.myX * myMapTileSizeInv) , (int) rint(aWorldPos.myY * myMapTileSizeInv) };
}

Vector2f World::ConvertTileToWorldCoords(Coord2i aTilePos)
{
	return{ static_cast<float>(aTilePos.myX * myMapTileSize), static_cast<float>(aTilePos.myY * myMapTileSize) };
}

size_t World::GetTileIndexForTileCoordinates(int aTileX, int aTileY) const
{
	return aTileY * myMapSizeX + aTileX;
}

Coord2i World::GetTileCoordsFromTileIndex(size_t aIndex) const
{
	return Coord2i(aIndex % myMapSizeX, aIndex / myMapSizeX);
}

bool World::LoadMap(ItemProvider* aItemProvider)
{
	std::string line;
	std::ifstream myfile ("map.txt");
	if (myfile.is_open())
	{
		int lineIndex = 0;
		myNumberOfDots = 0;
		while (! myfile.eof() )
		{
			std::getline (myfile,line);

			if (lineIndex == 0)
				myMapSizeX = line.length(); // assume all lines in the map file have same length 

			for (unsigned int i = 0; i < line.length(); i++)
			{
				if(i >= myMapSizeX)
					break;

				PathmapTile* tile = new PathmapTile(i, lineIndex, myMapTileSize);
				myNumberOfDots += tile->Init(aItemProvider, line[i]);				
				myPathmapTiles.push_back(tile);
			}

			lineIndex++;
		}

		myMapSizeY = lineIndex; // assume the map file doesnt have any other lines different from tiles 

		myfile.close();
	}

	return true;
}

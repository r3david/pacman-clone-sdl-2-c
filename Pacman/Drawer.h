//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef DRAWER_H
#define DRAWER_H

#include "TextureAsset.h"
#include "FontAsset.h"
#include "GameTime.h"

struct SDL_Window;
struct SDL_Renderer;
struct SDL_Surface;

class Gameplay;

// class that deal with all graphics and rendering of our game
class Drawer
{
public:
	Drawer();
	~Drawer(void);

	bool Init();
	void DeInit();

	static int ToScreenCoordinateX(int worldX) { return worldX + 220; }
	static int ToScreenCoordinateY(int worldY) { return worldY + 60; }
	
	void GameRender(Gameplay* aGame, GameTime* aTime);
	void Draw(TextureAsset* aTextureAsset, int screenX = 0, int screenY = 0);
	void DrawText(FontAsset* aFont, const char* aText, int aX, int aY);
		
	// accessors
	SDL_Renderer* GetRenderer() const { return myRenderer; }

private:
	SDL_Window* myWindow;
	SDL_Renderer* myRenderer;
};

#endif // DRAWER_H
//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "ScatterGhostBehaviour.h"
#include "ChaseGhostBehaviour.h"
#include "Ghost.h"
#include "Game.h"
#include "Utilities.h"

ScatterGhostBehaviour::ScatterGhostBehaviour()
{
	myBehaviourType = GhostBehaviourState::BehaviourType::Scatter;
}

ScatterGhostBehaviour::~ScatterGhostBehaviour()
{
}

void ScatterGhostBehaviour::OnBehaviourStart(Ghost * aGhost, World * aWorld)
{
	myPath.clear();
	myParams = &(aGhost->GetParams()->myScatterParams);
	myTimer = myParams->myTime;

	// path find our desired tile
	Coord2i start = aGhost->GetMotionComponent()->GetTilePos();
	Coord2i goal = myParams->myTargetTile;

	PathFinding::AStar::GetPath(start, goal, myPath, aWorld);

	aGhost->GetMotionComponent()->SetMotionSpeed(myParams->mySpeed);
	myScatterSgn = -1;
}

void ScatterGhostBehaviour::OnBehaviourUpdate(GameTime * aTime, Ghost * aGhost, Pacman * aPlayer, World * aWorld, GhostBehaviourFSM * aFSM)
{
	if (IsAtSameTileAsPlayer(aGhost, aPlayer, aWorld, aFSM))
	{
		Game::GetInstance().GetGameplayManager()->OnKilledByGhost();
		return;
	}

	myTimer -= aTime->DeltaTime();

	MotionComponent* motionComp = aGhost->GetMotionComponent();
	if (motionComp->IsCenteredAtCurrentTile())
	{
		if (myTimer <= 0)
		{
			myTimer = 0;
			aFSM->SetToState(BehaviourType::Chase, aWorld);
			return;
		}

		if (!ChaseGhostBehaviour::FollowPath(myDesiredMotionDir, aGhost, aWorld, myPath))
		{
			// here we have arrived at our target tile, but we still have some scatter time to wander around
			ChaseGhostBehaviour::GetWanderDirection(myDesiredMotionDir, aGhost, aWorld);
		}
	}

	motionComp->Move(myDesiredMotionDir);
}

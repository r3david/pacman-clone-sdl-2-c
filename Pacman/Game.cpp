//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "Game.h"
#include "SDL.h"
#include <iostream>

Game::Game()
{
}

bool Game::Init()
{
	myTime = new GameTime();
	myTime->Init();

	myDrawer = new Drawer();
	if(!myDrawer->Init())
		return false;

	myResourcesProvider = new ResourcesManager();
	if(!myResourcesProvider->Init())
		return false;

	myGameplayManager = new Gameplay();
	if(!myGameplayManager->Init(myResourcesProvider))
		return false;

	return true;
}

void Game::GameLoop()
{
	myGameplayManager->AtGameStart();

	SDL_Event event;
	while (SDL_PollEvent(&event) >= 0)
	{
		myTime->AtFrameStart();
		
		if(!myGameplayManager->ProcessInput())
			break;

		myGameplayManager->GameUpdate(myTime);

		myDrawer->GameRender(myGameplayManager, myTime);
	}
}

void Game::DeInit()
{
	myGameplayManager->DeInit();
	myResourcesProvider->DeInit();
	myDrawer->DeInit();

	delete myGameplayManager;
	delete myResourcesProvider;
	delete myDrawer;
	delete myTime;
}
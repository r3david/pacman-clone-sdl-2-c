//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include <sstream>
#include <fstream>
#include "GameplayUI.h"

GameplayUI::GameplayUI()
{
}

GameplayUI::~GameplayUI()
{
}

bool GameplayUI::Init(ResourcesManager * aResourceManager)
{
	if(aResourceManager == nullptr)
		return false;

	myUIFont = (FontAsset*) aResourceManager->GetResource("freefont-ttf\\sfd\\FreeMono.ttf",ResourcesManager::Font);

	return true;
}

void GameplayUI::DeInit()
{
}

void GameplayUI::Draw(Drawer* aDrawer, GameState aState, const GameData & aGameData, GameTime* aTime)
{
	DrawValue<int>(aDrawer, aGameData.myScore, "Score:", 0);
	DrawValue<int>(aDrawer, aGameData.myNLives, "Lives:", 1);
	DrawValue<int>(aDrawer, (int)aTime->GetFrameRate(), "FPS:", 2);
	
	if(aState == GameState::GameWin)
	{
		DrawValue<std::string>(aDrawer, ":)", "You win!", 3, 50);
	}
	else if(aState == GameState::GameOver)
	{
		DrawValue<std::string>(aDrawer, ":(", "You lose!", 3, 50);
	}
}

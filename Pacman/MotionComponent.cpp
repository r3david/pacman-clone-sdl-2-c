//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "MotionComponent.h"

MotionComponent::MotionComponent(GameEntity* aGameEntity, World* aWorld) :
	Component(aGameEntity),
	myWorld(aWorld),
	mySpeed(0.f),
	myNormalizedMotionDir({0.f,0.f})
{
	InitializeAtPosition(aGameEntity->GetPosition(), aWorld);
}

MotionComponent::~MotionComponent()
{
}

void MotionComponent::InitializeAtPosition(const Vector2f & pos, World* aWorld)
{
	myCurrentTilePos = aWorld->ConvertWorldToTileCoords(pos);
	myNextTilePos = myCurrentTilePos;
	myGameEntity->SetPosition(aWorld->ConvertTileToWorldCoords(myCurrentTilePos)); //in case we gave initial coords not at a single centered tile
	myIsCenteredAtTileFlag = true;
}

void MotionComponent::Move(Vector2f aInputDir)
{
	Coord2i targetTilePos(myCurrentTilePos.myX + static_cast<int>(aInputDir.myX), myCurrentTilePos.myY + static_cast<int>(aInputDir.myY));

	if (myWorld->IsTileWalkable(targetTilePos.myX, targetTilePos.myY))
	{
		float dotProdcut = Vector2f::Dot(myNormalizedMotionDir, aInputDir);
		if (dotProdcut == 0.0f) // when changing motion dir from vertical to horizontal or viceversa
		{
			//we have to be exactly in the middle of a tile
			if (myIsCenteredAtTileFlag)
				SetNextTile(targetTilePos);
		}
		else //changing of sign of direction can be done at any time
		{
			SetNextTile(targetTilePos);
		}
	}
}

void MotionComponent::Update(GameTime* aTime)
{
	float deltaTime = aTime->DeltaTime();
	size_t tileSize = myWorld->GetMapTileSize();
	float speed = mySpeed * static_cast<float>(tileSize); 

	myLastFrameMotionDir = myNormalizedMotionDir;
	myNormalizedMotionDir = Vector2f(static_cast<float>(myNextTilePos.myX - myCurrentTilePos.myX), static_cast<float>(myNextTilePos.myY - myCurrentTilePos.myY));
	Vector2f targetPos = myGameEntity->GetPosition() + myNormalizedMotionDir * speed * deltaTime;
	Vector2f nextTileInWorldCoords = myWorld->ConvertTileToWorldCoords(myNextTilePos);
	Vector2f worldMotionDir = nextTileInWorldCoords - targetPos;

	float dotProdcut = Vector2f::Dot(myNormalizedMotionDir, worldMotionDir); 
	if (dotProdcut < 0)
	{
		//we have supassed the nextTile! so we clamp to this tile
		myGameEntity->SetPosition(nextTileInWorldCoords);
		myCurrentTilePos = myNextTilePos;
		myIsCenteredAtTileFlag = true;

		//time to set the next tile from our new current tile according to our current motion dir
		//this gets overriden next frame in case the user is pressing into a direction of another walkable tile
		Move(myNormalizedMotionDir);
	}
	else
	{
		myIsCenteredAtTileFlag = (myCurrentTilePos == myNextTilePos);
		myGameEntity->SetPosition(targetPos);
	}
}

size_t MotionComponent::GetNumberOfDecisionsAtTile() const
{
	if(myIsCenteredAtTileFlag)
	{
		size_t nDecisions = myWorld->IsTileWalkable(myCurrentTilePos.myX + 1, myCurrentTilePos.myY) ? 1 : 0;
		nDecisions += myWorld->IsTileWalkable(myCurrentTilePos.myX - 1, myCurrentTilePos.myY) ? 1 : 0;
		nDecisions += myWorld->IsTileWalkable(myCurrentTilePos.myX, myCurrentTilePos.myY + 1) ? 1 : 0;
		nDecisions += myWorld->IsTileWalkable(myCurrentTilePos.myX, myCurrentTilePos.myY - 1) ? 1 : 0;
		return nDecisions;
	}

	return 0;
}

//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef GHOST_BEHAVIOUR_FSM
#define GHOST_BEHAVIOUR_FSM

#include "GhostBehaviourState.h"
#include <vector>

class GhostBehaviourFSM
{
public:
	GhostBehaviourFSM();
	~GhostBehaviourFSM();

	bool InitFSM(Ghost* aGhost);
	void DeInit();
	void StartOnState(GhostBehaviourState::BehaviourType aType, World* aWorld);
	void SetToState(GhostBehaviourState::BehaviourType aType, World* aWorld);
	void ResumeBehaviourFromVulnerable(World* aWorld);
	void Update(GameTime* aTime, Pacman* aPlayer, World* aWorld);
	GhostBehaviourState::BehaviourType GetCurrentBehaviour() const;

private:
	Ghost* myGhostRef;
	GhostBehaviourState* myCurrentState;
	std::vector<GhostBehaviourState*> myStatesTable;
};

#endif // GHOST_BEHAVIOUR_FSM

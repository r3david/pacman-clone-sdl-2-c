//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "Gameplay.h"

Gameplay::Gameplay()
{
}

Gameplay::~Gameplay()
{
}

bool Gameplay::Init(ResourcesManager* aResourcesProvider)
{
	myPacmanConfiguration = PacManDef::CreateNewPacManDef();
	myScoresDefinitions = ScoresDef::CreateScoresDef();
	myEnemyDefinitions = EnemiesDef::CreateNewEnemiesDef();
	myCurrentGameData = GameData::CreateNewGameData();

	myItemProvider = new ItemProvider();
	if (!myItemProvider->Init(aResourcesProvider))
		return false;

	myWorld = new World();
	if (!myWorld->Init(aResourcesProvider, myItemProvider))
		return false;

	myPlayer = new Pacman();
	if (!myPlayer->Init(myWorld, aResourcesProvider))
		return false;

	myEnemies = new EnemiesManager();
	if (!myEnemies->Init(&myEnemyDefinitions, myWorld, aResourcesProvider))
		return false;

	myUI = new GameplayUI();
	if(!myUI->Init(aResourcesProvider))
		return false;

	return true;
}

void Gameplay::DeInit()
{
	myUI->DeInit();
	myEnemies->DeInit();
	myPlayer->DeInit();
	myWorld->DeInit();
	myItemProvider->DeInit();

	delete myUI;
	delete myEnemies;
	delete myPlayer;
	delete myWorld;
	delete myItemProvider;
}

bool Gameplay::ProcessInput()
{
	const Uint8 *keystate = SDL_GetKeyboardState(nullptr);

	if (keystate[SDL_SCANCODE_ESCAPE])
		return false;

	if (myGameState == GameState::Playing)
		myPlayer->ProcessInput(keystate);

	return true;
}

void Gameplay::AtGameStart()
{
	myPlayer->Respawn(myPacmanConfiguration, myWorld);
	myEnemies->AtGameStarts(myWorld);

	myGameState = GameState::Playing;
}

void Gameplay::GameUpdate(GameTime* aTime)
{
	if (myGameState != GameState::Playing)
		return;
	
	// update player motion and animation
	myPlayer->Update(aTime);

	// update enemies
	myEnemies->Update(aTime, myPlayer, myWorld);

	// check for dots and other pickups
	Coord2i playerTileCoords = myPlayer->GetMotionComponent().GetTilePos();
	PathmapTile* playerTile = myWorld->GetTile(playerTileCoords.myX, playerTileCoords.myY);
	playerTile->GetTileItem()->OnPickedUp(playerTile);
}

void Gameplay::OnDotPickedUp(PathmapTile* aTile)
{
	myCurrentGameData.myScore += myScoresDefinitions.scoreForDot;
	myCurrentGameData.myNumberOfDots++;
	aTile->OnItemPickedUp(myItemProvider);
	CheckGameWin();
}

void Gameplay::OnBigDotPickedUp(PathmapTile * aTile)
{
	myCurrentGameData.myScore += myScoresDefinitions.scoreForBigDot;
	myCurrentGameData.myNumberOfDots++;
	aTile->OnItemPickedUp(myItemProvider);
	
	if(!CheckGameWin())
		myEnemies->OnGhostsBecomeVulnerable(myWorld);
}

void Gameplay::OnCherryPickedUp(PathmapTile * aTile)
{
	// TODO ..

	aTile->OnItemPickedUp(myItemProvider);
}

void Gameplay::OnGhostKilled()
{
	myCurrentGameData.myScore += myScoresDefinitions.scoreForVulnerableGhost;
}

void Gameplay::OnKilledByGhost()
{
	myCurrentGameData.myNLives--;

	if(myCurrentGameData.myNLives <= 0)
	{
		OnGameOver();
	}
	else
	{
		// restart pacman position and ghosts
		AtGameStart();
	}
}

void Gameplay::OnGameWin()
{
	myGameState = GameState::GameWin;
}

void Gameplay::OnGameOver()
{
	myGameState = GameState::GameOver;
}

bool Gameplay::CheckGameWin()
{
	if(myCurrentGameData.myNumberOfDots == myWorld->GetNumberOfDotsInWorld())
	{
		OnGameWin();
		return true;
	}
	return false;
}

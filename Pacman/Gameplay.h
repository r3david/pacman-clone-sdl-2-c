//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "GameData.h"
#include "PacmanDef.h"
#include "ScoresDef.h"
#include "World.h"
#include "Pacman.h"
#include "GameTime.h"
#include "GameplayUI.h"
#include "EnemiesManager.h"

class Gameplay
{
public:
	Gameplay();
	~Gameplay();

	bool Init(ResourcesManager* aResourcesProvider);
	void DeInit();
	
	// game logic
	bool ProcessInput();
	void AtGameStart();
	void GameUpdate(GameTime* aTime);

	// events
	void OnDotPickedUp(PathmapTile* aTile);
	void OnBigDotPickedUp(PathmapTile* aTile);
	void OnCherryPickedUp(PathmapTile* aTile);
	void OnGhostKilled();
	void OnKilledByGhost();
	void OnGameWin();
	void OnGameOver();
	bool CheckGameWin();

	// accessors
	ItemProvider* GetItemProvider() const { return myItemProvider; }
	World* GetWorld() const { return myWorld; }
	Pacman* GetPlayer() const { return myPlayer; }
	GameplayUI* GetGameplayUI() const { return myUI; }
	EnemiesManager* GetEnemiesManager() const { return myEnemies; }
	GameState GetGameState() const { return myGameState; }
	const GameData& GetCurrentGameData() const{ return myCurrentGameData; }

private:
	PacManDef myPacmanConfiguration;
	ScoresDef myScoresDefinitions;
	EnemiesDef myEnemyDefinitions;
	GameData myCurrentGameData;
	GameState myGameState;

	ItemProvider* myItemProvider;
	World* myWorld;
	Pacman* myPlayer;
	GameplayUI* myUI;
	EnemiesManager* myEnemies;
};

#endif // GAMEPLAY_H
//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef MOTIONCOMPONENT_H
#define MOTIONCOMPONENT_H

#include "Component.h"
#include "Vector2f.h"
#include "Coord2i.h"
#include "World.h"
#include "GameTime.h"

class MotionComponent : public Component 
{
public:
	MotionComponent(GameEntity* aGameEntity, World* aWorld);
	~MotionComponent();

	void InitializeAtPosition(const Vector2f & pos, World* aWorld);
	void Move(Vector2f aInputDir);
	void Update(GameTime* aTime);
	size_t GetNumberOfDecisionsAtTile() const;

	inline void SetMotionSpeed(float aSpeed) { mySpeed = aSpeed; }
	inline Vector2f GetMotionDir() const { return myNormalizedMotionDir; }
	inline bool HasChangedDirection() const { return myNormalizedMotionDir != myLastFrameMotionDir; }
	inline Coord2i GetTilePos() const { return myCurrentTilePos; }
	inline bool IsCenteredAtCurrentTile() const { return myIsCenteredAtTileFlag; }
	inline Coord2i GetNextTilePosForDirection(const Vector2f & dir) const { return myCurrentTilePos + Coord2i(static_cast<int>(dir.myX), static_cast<int>(dir.myY)); }
	inline Coord2i GetNextTilePosForMyMotionDir() const { return GetNextTilePosForDirection(myNormalizedMotionDir); }
	
private:
	inline void SetNextTile(Coord2i aNextTilePos) { myNextTilePos = aNextTilePos; }

	Coord2i myCurrentTilePos;
	Coord2i myNextTilePos;
	Vector2f myNormalizedMotionDir;
	Vector2f myLastFrameMotionDir;
	World* myWorld;
	float mySpeed; //in tiles per seconds
	bool myIsCenteredAtTileFlag;
};

#endif // MOTIONCOMPONENT_H
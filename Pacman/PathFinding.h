//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef PATH_FIND_H
#define PATH_FIND_H

#include "World.h"
#include <list>

namespace PathFinding
{
	struct FrontierPoint {
	public:
		size_t priority;
		Coord2i point;

		bool operator <(const FrontierPoint & other) const {
			return priority < other.priority;
		}

		bool operator >(const FrontierPoint & other) const {
			return priority > other.priority;
		}
	};

	class AStar
	{
	public:
		static void GetPath(Coord2i start, Coord2i goal, std::list<Coord2i>& aPath, World * aWorld);
		static size_t ManhattanDistance(const Coord2i & a, const Coord2i & b);
	};

	class Dijkstra
	{
	public:
		static void GetPath(int aFromX, int aFromY, int aToX, int aToY, std::list<PathmapTile*>& aList, World* aWorld);

	private:
		static bool Pathfind(PathmapTile* aFromTile, PathmapTile* aToTile, std::list<PathmapTile*>& aList, World* aWorld);
		static bool ListDoesNotContain(PathmapTile* aFromTile, std::list<PathmapTile*>& aList);
	};

}



#endif // PATH_FIND_H

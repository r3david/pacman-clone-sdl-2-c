//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "Drawer.h"
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "Game.h"
#include "assert.h"

Drawer::Drawer()
{
}

Drawer::~Drawer(void)
{
}

bool Drawer::Init()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		assert(0 && "Failed to initialize video!");
		return false;
	}

	myWindow = SDL_CreateWindow("Pacman", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 768, SDL_WINDOW_OPENGL);
	myRenderer = SDL_CreateRenderer(myWindow, -1, SDL_RENDERER_ACCELERATED);

	if (!myWindow)
	{
		assert(0 && "Failed to create window!");
		return false;
	}

	if (!myRenderer)
	{
		assert(0 && "Failed to create renderer!");
		return false;
	}

	IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);

	if (TTF_Init() == -1)
	{
		assert(0 && "Failed to create ttf!");
		return false;
	}

	return true;
}

void Drawer::DeInit()
{
	SDL_DestroyRenderer(myRenderer);
	SDL_DestroyWindow(myWindow);

	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

void Drawer::GameRender(Gameplay* aGame, GameTime* aTime)
{
	SDL_SetRenderDrawColor(myRenderer, 0, 0, 0, 255);
	SDL_RenderClear(myRenderer);

	aGame->GetWorld()->Draw(this);
	aGame->GetPlayer()->Draw(this);
	aGame->GetEnemiesManager()->Draw(this);
	aGame->GetGameplayUI()->Draw(this, aGame->GetGameState(), aGame->GetCurrentGameData(), aTime);

	SDL_RenderPresent(myRenderer);
}

void Drawer::Draw(TextureAsset* aTextureAsset, int screenX, int screenY)
{
	if(aTextureAsset == nullptr)
		return;

	const SDL_Rect& sizeRect = aTextureAsset->GetSizeRect();

	//transform to screen coordinates 
	SDL_Rect posRect;
	posRect.x = screenX;
	posRect.y = screenY;
	posRect.w = sizeRect.w;
	posRect.h = sizeRect.h;

	SDL_RenderCopy(myRenderer, aTextureAsset->GetTexture(), &sizeRect, &posRect);
}

void Drawer::DrawText(FontAsset * aFont, const char * aText, int aX, int aY)
{
	SDL_Color fg={255,0,0,255};
	SDL_Surface* surface = TTF_RenderText_Solid(aFont->GetFont(), aText, fg);
	
	SDL_Texture* optimizedSurface = SDL_CreateTextureFromSurface(myRenderer, surface);
	
	SDL_Rect sizeRect;
	sizeRect.x = 0 ;
	sizeRect.y = 0 ;
	sizeRect.w = surface->w ;
	sizeRect.h = surface->h ;
	
	SDL_Rect posRect ;
	posRect.x = aX;
	posRect.y = aY;
	posRect.w = sizeRect.w;
	posRect.h = sizeRect.h;
	
	SDL_RenderCopy(myRenderer, optimizedSurface, &sizeRect, &posRect);

	SDL_DestroyTexture(optimizedSurface);
	SDL_FreeSurface(surface);
}

//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef RESOURCE_DATABASE_H
#define RESOURCE_DATABASE_H

#include <unordered_map>

template<class T>
class ResourceDatabase
{
public:
	ResourceDatabase(){}
	~ResourceDatabase(){}

	void* GetResource(const char* aResourceName)  
	{
		const auto & iter = myResourcesDB.find(aResourceName);
		if (iter != myResourcesDB.end())
		{
			return iter->second;
		}

		void* asset = (void*) new T(aResourceName);
		myResourcesDB.insert({ aResourceName, asset });
		return asset;
	}

	void DeInit()
	{
		for (auto & entry : myResourcesDB)
		{
			if (entry.second != nullptr)
				delete entry.second;
		}
	}

private:
	std::unordered_map<std::string, void*> myResourcesDB;
};

#endif //RESOURCE_DATABASE_H

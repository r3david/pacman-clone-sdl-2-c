//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "Pacman.h"
#include "Drawer.h"
#include "SDL.h"
#include <iostream>

Pacman::Pacman() :
	GameEntity(Vector2f(0.f,0.f)),
	myInputDirection(0.f, 0.f)
{

}

Pacman::~Pacman()
{
}

bool Pacman::Init(World* aWorld, ResourcesManager* aResourcesManager)
{
	if(aWorld == nullptr || aResourcesManager == nullptr)
		return false;

	if(!InitPacmanAnimations(aResourcesManager))
		return false;

	if(!InitPacmanMovement(aWorld))
		return false;

	return true;
}

void Pacman::DeInit()
{
	delete myMotionComponent;
	delete myAnimatorRenderer; 
}

void Pacman::Draw(Drawer* aDrawer)
{
	myAnimatorRenderer->Render(aDrawer);
}

void Pacman::Respawn(const PacManDef & def, World* aWorld)
{
	myInputDirection = def.myStartMotionDir;
	myMotionComponent->InitializeAtPosition(def.mySpawnPosition * static_cast<float>(aWorld->GetMapTileSize()), aWorld);
	myMotionComponent->SetMotionSpeed(def.myTileSpeed);
}

void Pacman::ProcessInput(const Uint8 * aKeystate)
{
	if (aKeystate[SDL_SCANCODE_UP])
		myInputDirection = Vector2f(0.f, -1.f);
	else if (aKeystate[SDL_SCANCODE_DOWN])
		myInputDirection = Vector2f(0.f, 1.f);
	else if (aKeystate[SDL_SCANCODE_RIGHT])
		myInputDirection = Vector2f(1.f, 0.f);
	else if (aKeystate[SDL_SCANCODE_LEFT])
		myInputDirection = Vector2f(-1.f, 0.f);
}

void Pacman::Update(GameTime* aTime)
{
	// movement
	myMotionComponent->Move(myInputDirection);
	myMotionComponent->Update(aTime);

	//animation
	if(myMotionComponent->HasChangedDirection())
		UpdateAnimationAccordingToDirection(myMotionComponent->GetMotionDir());
	
	myAnimatorRenderer->Update(aTime);
}

bool Pacman::InitPacmanAnimations(ResourcesManager* aResourcesManager)
{
	myAnimatorRenderer = new SpriteAnimatorComponent(this);

	// animation config
	float animationFrameRate = 6.f;

	//initialise animations
	TextureAsset* textureAsset1, *textureAsset2;
	SpriteAnimation* animation;

	std::vector<std::string> pacmanMoves;
	pacmanMoves.push_back("right");
	pacmanMoves.push_back("left");
	pacmanMoves.push_back("up");
	pacmanMoves.push_back("down");

	for(auto & move : pacmanMoves)
	{
		std::string openMove = "open_" + move + "_32.png";
		std::string closedMove = "closed_" + move + "_32.png";
		textureAsset1 = (TextureAsset*)aResourcesManager->GetResource(openMove.c_str(), ResourcesManager::Texture);
		textureAsset2 = (TextureAsset*)aResourcesManager->GetResource(closedMove.c_str(), ResourcesManager::Texture);

		std::string animationName = "move_" + move;
		animation = (SpriteAnimation*)aResourcesManager->GetResource(animationName.c_str(), ResourcesManager::Animation);
		
		animation->AddSpriteFrame(textureAsset1);
		animation->AddSpriteFrame(textureAsset2);
		animation->SetFramesPerSecond(animationFrameRate);

		myAnimatorRenderer->AddAnimation(animation);
	}

	return true;
}

bool Pacman::InitPacmanMovement(World* aWorld)
{
	myMotionComponent = new MotionComponent(this, aWorld);
	return true;
}

void Pacman::UpdateAnimationAccordingToDirection(const Vector2f & dir)
{
	if (dir.myX != 0) //left or right
	{
		if (dir.myX > 0)
			myAnimatorRenderer->Play("move_right");
		else if (dir.myX < 0)
			myAnimatorRenderer->Play("move_left");
	}
	else //up or down
	{
		if (dir.myY > 0)
			myAnimatorRenderer->Play("move_down");
		else if (dir.myY < 0)
			myAnimatorRenderer->Play("move_up");
	}
}



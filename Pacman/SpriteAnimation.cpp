//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "SpriteAnimation.h"
#include "Utilities.h"

SpriteAnimation::SpriteAnimation(const std::string & aAnimationName) : 
	myAnimationName(aAnimationName)
{
	myHashedName = Utilities::StringToHash(aAnimationName.c_str());
}

SpriteAnimation::~SpriteAnimation()
{
}

bool SpriteAnimation::operator==(const SpriteAnimation& other) const
{
	return myHashedName == other.myHashedName;
}

void SpriteAnimation::SetFramesPerSecond(float aFrameRate)
{
	float frameDuration = 1.f / aFrameRate;
	myAnimationDuration = frameDuration * static_cast<float>(myAnimationTextures.size()); 
}

void SpriteAnimation::AddSpriteFrame(TextureAsset* aTexture)
{
	myAnimationTextures.push_back(aTexture);
}

void SpriteAnimation::OnAnimationStart(float aTime, size_t frame)
{
	size_t nFrames = myAnimationTextures.size();
	float frameDuration = myAnimationDuration / static_cast<float>(nFrames);

	myCurrentFrame = frame % nFrames;
	myPlaybackTime = aTime - static_cast<float>(frame) * frameDuration;;
}

void SpriteAnimation::OnAnimationUpdate(float aTime)
{
	float elapsedTime = aTime - myPlaybackTime;
	size_t nFrames = myAnimationTextures.size();
	myCurrentFrame = (size_t) ( (elapsedTime / myAnimationDuration) * static_cast<float>(nFrames));
	myCurrentFrame %= nFrames;
}

TextureAsset * SpriteAnimation::GetCurrentAnimationFrame() const
{
	return myAnimationTextures[myCurrentFrame]; 
}

size_t SpriteAnimation::GetCurrentFrameIndex() const
{
	return myCurrentFrame;
}

const std::string & SpriteAnimation::GetAnimationName() const
{
	return myAnimationName;
}

//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef PACMANGAME_H
#define PACMANGAME_H

#include "GameTime.h"
#include "drawer.h"
#include "ResourcesManager.h"
#include "ItemProvider.h"
#include "World.h"
#include "Pacman.h"
#include "Gameplay.h"

// The game class that initializes and runs the game loop
// it also serves as a global service locator for Assets, Drawer, Player, etc.
// instead of having multiple singleton classes each for Drawer, TextureAssetDatabase etc.., 
// I prefer to have just 1 singleton (this class) from where I can access the other services, 
// so we can swap the implementation of a particular service easily if we wanted
class Game
{
public:
	// delete unwanted CTORS
	Game(Game const&) = delete;
	void operator=(Game const&) = delete;

	// Main Game methods
	bool Init();
	void GameLoop();
	void DeInit();

	// service accesors
	static Game& GetInstance()
	{
		static Game instance; // guaranteed to be destroyed (better than having an Game* instance, since the user could attempt to delete the Game pointer)
		return instance; //Instantiated on first use, which we do at the main start
	}

	GameTime* GetTime() const { return myTime; }
	Drawer* GetDrawer() const { return myDrawer; }
	ResourcesManager* GetResourcesProvider() const { return myResourcesProvider; }
	Gameplay* GetGameplayManager() const { return myGameplayManager; }

private:
	Game();
	
private:
	GameTime* myTime;
	Drawer* myDrawer; 
	ResourcesManager* myResourcesProvider;
	Gameplay* myGameplayManager;
};

#endif // PACMANGAME_H
//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "RestGhostBehaviour.h"
#include "Ghost.h"

RestGhostBehaviour::RestGhostBehaviour()
{
	myBehaviourType = BehaviourType::AtRest;
}

RestGhostBehaviour::~RestGhostBehaviour()
{
}

void RestGhostBehaviour::OnBehaviourStart(Ghost * aGhost, World * aWorld)
{
	myTimer = aGhost->GetParams()->myRestParams.myTime;
	
	aGhost->GetMotionComponent()->SetMotionSpeed(2.f); 
	myDesiredMotionDir = Vector2f(-1.f, 0); //at rest we move always left and right
}

void RestGhostBehaviour::OnBehaviourUpdate(GameTime * aTime, Ghost * aGhost, Pacman * aPlayer, World * aWorld, GhostBehaviourFSM * aFSM)
{
	myTimer -= aTime->DeltaTime();

	MotionComponent* motionComp = aGhost->GetMotionComponent();
	if (motionComp->IsCenteredAtCurrentTile())
	{
		if (myTimer <= 0)
		{
			aGhost->OnFinishedResting(aWorld);
			return;
		}

		myDesiredMotionDir = Vector2f(-myDesiredMotionDir.myY, myDesiredMotionDir.myX);
	}
	
	motionComp->Move(myDesiredMotionDir);
}

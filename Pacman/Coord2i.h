//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef COORD2I_H
#define COORD2I_H

struct Coord2i
{
public:
	int myX;
	int myY;

	Coord2i() : myX (0), myY(0){}
	Coord2i(int aX, int aY) : myX(aX), myY(aY){}

	const Coord2i Coord2i::operator-(const Coord2i &other) const
	{
		return {myX - other.myX, myY - other.myY };
	}

	const Coord2i Coord2i::operator+(const Coord2i &other) const
	{
		return{ myX + other.myX, myY + other.myY };
	}

	const Coord2i Coord2i::operator*(const int & scalar) const
	{
		return {myX * scalar, myY * scalar };
	}

	const bool Coord2i::operator==(const Coord2i & other) const
	{
		return myX == other.myX && myY == other.myY; 
	}

	const bool Coord2i::operator!=(const Coord2i & other) const
	{
		return myX != other.myX || myY != other.myY;
	}
};

#endif // COORD2I_H
//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef GAMEPLAY_UI_H
#define GAMEPLAY_UI_H

#include "ResourcesManager.h"
#include "Drawer.h"
#include "GameData.h"
#include "GameTime.h"

class GameplayUI
{
public:
	GameplayUI();
	~GameplayUI();

	bool Init(ResourcesManager* aResourceManager);
	void DeInit();
	void Draw(Drawer* aDrawer, GameState aState, const GameData & aGameData, GameTime* aTime);

private:
	template <class T>
	void DrawValue(Drawer* aDrawer, T aValue, const char* aTitle, int aYOffset, int aXOffset = 0) const;

	FontAsset* myUIFont;
};

template<class T>
inline void GameplayUI::DrawValue(Drawer * aDrawer, T aValue, const char * aTitle, int aYOffset, int aXOffset) const
{
	std::string textString;
	std::stringstream textStream;

	int y = 50 + 30 * aYOffset;

	textStream << aValue;
	textString = textStream.str();
	aDrawer->DrawText(myUIFont, aTitle, 20, y);
	aDrawer->DrawText(myUIFont, textString.c_str(), 110 + aXOffset, y);
}

#endif // GAMEPLAY_UI_H


//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "DeadGhostBehaviour.h"
#include "PathFinding.h"
#include "Ghost.h"
#include "Pacman.h"
#include "ChaseGhostBehaviour.h"

DeadGhostBehaviour::DeadGhostBehaviour()
{
	myBehaviourType = GhostBehaviourState::BehaviourType::Dead;
}

DeadGhostBehaviour::~DeadGhostBehaviour()
{
}

void DeadGhostBehaviour::OnBehaviourStart(Ghost* aGhost, World * aWorld)
{
	GhostParams* params = aGhost->GetParams();

	myPath.clear();
	myTargetPos = params->mySpawnPosition;

	aGhost->GetMotionComponent()->SetMotionSpeed(2.f * params->myChaseParams.mySpeed);
	
	Coord2i start = aGhost->GetMotionComponent()->GetTilePos();
	Coord2i goal = Coord2i(static_cast<int>(myTargetPos.myX), static_cast<int>(myTargetPos.myY));

	PathFinding::AStar::GetPath(start, goal, myPath, aWorld);
}

void DeadGhostBehaviour::OnBehaviourUpdate(GameTime * aTime, Ghost * aGhost, Pacman * aPlayer, World * aWorld, GhostBehaviourFSM * aFSM)
{
	MotionComponent* motionComp = aGhost->GetMotionComponent();

	if (motionComp->IsCenteredAtCurrentTile()) 
	{
		// follow to spawn position, then swap to rest to start again
		if (!ChaseGhostBehaviour::FollowPath(myDesiredMotionDir, aGhost, aWorld, myPath))
		{
			aGhost->OnVulnerableTimerFinishes(aWorld);
			aFSM->SetToState(BehaviourType::AtRest, aWorld);
		}
	}

	aGhost->GetMotionComponent()->Move(myDesiredMotionDir);
}

//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "ItemProvider.h"
#include "NullItem.h"
#include "Dot.h"
#include "BigDot.h"
#include "Cherry.h"

ItemProvider::ItemProvider()
{
}

ItemProvider::~ItemProvider()
{
}

bool ItemProvider::Init(ResourcesManager* aResourceManager)
{
	if(aResourceManager == nullptr)
		return false;

	unsigned int nItems = static_cast<unsigned int>(Item::Cherry) + 1;
	myItemsDB.resize(nItems);

	myItemsDB[static_cast<unsigned int>(Item::NullItem)] = new NullItem();
	myItemsDB[static_cast<unsigned int>(Item::Dot)]	= new Dot((TextureAsset*) aResourceManager->GetResource("Small_Dot_32.png",ResourcesManager::Texture));
	myItemsDB[static_cast<unsigned int>(Item::BigDot)] = new BigDot((TextureAsset*)aResourceManager->GetResource("Big_Dot_32.png", ResourcesManager::Texture));
	myItemsDB[static_cast<unsigned int>(Item::Cherry)]= new Cherry((TextureAsset*)aResourceManager->GetResource("dot.png", ResourcesManager::Texture));

	return true;
}

Item * ItemProvider::GetItemInstance(Item::ItemType aType) const
{
	return myItemsDB[static_cast<unsigned int>(aType)];
}

void ItemProvider::DeInit()
{
	for(auto & item : myItemsDB)
	{
		delete item;
	}
}

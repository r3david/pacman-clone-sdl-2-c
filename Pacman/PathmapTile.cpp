//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "PathmapTile.h"

PathmapTile::PathmapTile(int aTileX, int aTileY, int aTileSize) :
	myX(aTileX),
	myY(aTileY),
	myWorldX(aTileX * aTileSize),
	myWorldY(aTileY * aTileSize),
	myIsBlockingFlag(false),
	myIsVisitedFlag(false)
{
}

PathmapTile::~PathmapTile(void)
{
}

size_t PathmapTile::Init(ItemProvider* aItemProdiver, const char aTileChar)
{
	myIsBlockingFlag = aTileChar == 'x';

	switch (aTileChar)
	{
		case '.': myTileItem = aItemProdiver->GetItemInstance(Item::Dot); return 1;
		case 'o': myTileItem = aItemProdiver->GetItemInstance(Item::BigDot); return 1;
		default : myTileItem = aItemProdiver->GetItemInstance(Item::NullItem); return 0;
	}

	return 0;
}

void PathmapTile::Draw(Drawer* aDrawer) const
{
	aDrawer->Draw(myTileItem->GetTextureAsset(), Drawer::ToScreenCoordinateX(myWorldX), Drawer::ToScreenCoordinateY(myWorldY));
}

void PathmapTile::OnItemPickedUp(ItemProvider * aItemProvider)
{
	myTileItem = aItemProvider->GetItemInstance(Item::NullItem);
}

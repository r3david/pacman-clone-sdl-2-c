//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "GhostBehaviourFSM.h"
#include "ChaseGhostBehaviour.h"
#include "DeadGhostBehaviour.h"
#include "ScatterGhostBehaviour.h"
#include "RestGhostBehaviour.h"
#include "VulnerableGhostBehaviour.h"

GhostBehaviourFSM::GhostBehaviourFSM()
{
}

GhostBehaviourFSM::~GhostBehaviourFSM()
{
}

bool GhostBehaviourFSM::InitFSM(Ghost* aGhost)
{
	myGhostRef = aGhost;

	myStatesTable.resize((int)GhostBehaviourState::BehaviourType::Dead + 1);
	myStatesTable[(int)GhostBehaviourState::BehaviourType::AtRest]		= new RestGhostBehaviour();
	myStatesTable[(int)GhostBehaviourState::BehaviourType::Chase]		= new ChaseGhostBehaviour();
	myStatesTable[(int)GhostBehaviourState::BehaviourType::Scatter]		= new ScatterGhostBehaviour();
	myStatesTable[(int)GhostBehaviourState::BehaviourType::Vulnerable]	= new VulnerableGhostBehaviour();
	myStatesTable[(int)GhostBehaviourState::BehaviourType::Dead]		= new DeadGhostBehaviour();

	return true;
}

void GhostBehaviourFSM::DeInit()
{
	for(GhostBehaviourState*& behaviour : myStatesTable)
		delete behaviour;
}

void GhostBehaviourFSM::StartOnState(GhostBehaviourState::BehaviourType aType, World* aWorld)
{
	myCurrentState = myStatesTable[(int)aType];
	myCurrentState->OnBehaviourStart(myGhostRef, aWorld);
}

void GhostBehaviourFSM::SetToState(GhostBehaviourState::BehaviourType aType, World* aWorld)
{
	GhostBehaviourState* newState = myStatesTable[(int)aType];

	if (myCurrentState != newState)
	{
		myCurrentState = newState;
		myCurrentState->OnBehaviourStart(myGhostRef, aWorld);
	}
	else if (aType == GhostBehaviourState::BehaviourType::Vulnerable)
	{
		// add more vulnerable time to this ghost
		VulnerableGhostBehaviour* beh = dynamic_cast<VulnerableGhostBehaviour*>(myCurrentState);
		if (beh != nullptr)
			beh->AddVulnerableTime();
	}
}

void GhostBehaviourFSM::ResumeBehaviourFromVulnerable(World * aWorld)
{
	if(myCurrentState->GetBehaviourType() == GhostBehaviourState::BehaviourType::Vulnerable)
	{
		SetToState(GhostBehaviourState::BehaviourType::Scatter, aWorld);
	}
}

void GhostBehaviourFSM::Update(GameTime * aTime, Pacman * aPlayer, World * aWorld)
{
	myCurrentState->OnBehaviourUpdate(aTime, myGhostRef, aPlayer, aWorld, this);
}

GhostBehaviourState::BehaviourType GhostBehaviourFSM::GetCurrentBehaviour() const
{
	return myCurrentState->GetBehaviourType();
}

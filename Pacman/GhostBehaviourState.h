//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef GHOST_BEHAVIOUR_STATE_H
#define GHOST_BEHAVIOUR_STATE_H

#include "GameTime.h"
#include "EnemiesDef.h"
#include "PathFinding.h"

class Ghost;
class World;
class Pacman;
class GhostBehaviourFSM;

// Base class for all ghosts behaviours, 
// we will have a different behaviour for being at rest, chasing and scatter, 
// and two more behaviours for when being vulnerable and dead
class GhostBehaviourState
{
public:
	enum BehaviourType : int
	{
		AtRest = 0,
		Chase,
		Scatter,
		Vulnerable,
		Dead,
	};

	virtual ~GhostBehaviourState(){};
	virtual void OnBehaviourStart(Ghost* ghost, World * aWorld) = 0;
	virtual void OnBehaviourUpdate(GameTime* aTime, Ghost* ghost, Pacman* aPlayer, World* aWorld, GhostBehaviourFSM* aFSM) = 0;
	bool IsAtSameTileAsPlayer(Ghost* ghost, Pacman* aPlayer, World* aWorld, GhostBehaviourFSM* aFSM);

	BehaviourType GetBehaviourType() const { return myBehaviourType; }

	const bool GhostBehaviourState::operator==(const GhostBehaviourState& other) const
	{
		return myBehaviourType == other.myBehaviourType;
	}

	const bool GhostBehaviourState::operator!=(const GhostBehaviourState& other) const
	{
		return myBehaviourType != other.myBehaviourType;
	}

protected:
	GhostBehaviourState(){};

	BehaviourType myBehaviourType;
	Vector2f myDesiredMotionDir;
	std::list<Coord2i> myPath;
};

#endif // !GHOST_BEHAVIOUR_STATE_H


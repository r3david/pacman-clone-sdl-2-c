//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "VulnerableGhostBehaviour.h"
#include "Ghost.h"
#include "Pacman.h"
#include "Game.h"
#include "Utilities.h"

VulnerableGhostBehaviour::VulnerableGhostBehaviour()
{
	myBehaviourType = GhostBehaviourState::BehaviourType::Vulnerable;
}

VulnerableGhostBehaviour::~VulnerableGhostBehaviour()
{
}

void VulnerableGhostBehaviour::OnBehaviourStart(Ghost * aGhost, World * aWorld)
{
	myParams = &(aGhost->GetParams()->myVulnerableParams);
	myTimer = myParams->vulnerableTime;
	aGhost->GetMotionComponent()->SetMotionSpeed(myParams->mySpeed);
	myPath.clear();
	myDesiredMotionDir = aGhost->GetMotionComponent()->GetMotionDir() * -1.f;
}

void VulnerableGhostBehaviour::OnBehaviourUpdate(GameTime * aTime, Ghost * aGhost, Pacman * aPlayer, World * aWorld, GhostBehaviourFSM * aFSM)
{
	if (IsAtSameTileAsPlayer(aGhost, aPlayer, aWorld, aFSM))
	{
		Game::GetInstance().GetGameplayManager()->OnGhostKilled();
		aGhost->OnGhostKilled(aWorld);
		return;
	}

	myTimer -= aTime->DeltaTime();

	MotionComponent* motionComp = aGhost->GetMotionComponent();	
	if (motionComp->IsCenteredAtCurrentTile())
	{
		if (myTimer <= 0)
		{
			myTimer = 0;
			aGhost->OnVulnerableTimerFinishes(aWorld);
			return;
		}

		ChaseGhostBehaviour::GetWanderDirection(myDesiredMotionDir, aGhost, aWorld);
	}

	motionComp->Move(myDesiredMotionDir);
}

void VulnerableGhostBehaviour::AddVulnerableTime()
{
	if (myTimer > 0 && myParams != nullptr)
		myTimer += myParams->vulnerableTime;
}

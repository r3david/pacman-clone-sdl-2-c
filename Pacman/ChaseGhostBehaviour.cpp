//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "ChaseGhostBehaviour.h"
#include "Game.h"
#include "Pacman.h"
#include "Ghost.h"
#include "Utilities.h"

ChaseGhostBehaviour::ChaseGhostBehaviour()
{
	myBehaviourType = BehaviourType::Chase;
}

ChaseGhostBehaviour::~ChaseGhostBehaviour()
{
}

void ChaseGhostBehaviour::OnBehaviourStart(Ghost * aGhost, World * aWorld)
{
	myParams = &(aGhost->GetParams()->myChaseParams);

	aGhost->GetMotionComponent()->SetMotionSpeed(myParams->mySpeed);
	myDesiredMotionDir = myParams->myInitialDesiredMotionDir;
	myTimer = myParams->myTime;

	myPath.clear();
}

void ChaseGhostBehaviour::OnBehaviourUpdate(GameTime * aTime, Ghost * aGhost, Pacman* aPlayer, World* aWorld, GhostBehaviourFSM* aFSM)
{
	if (IsAtSameTileAsPlayer(aGhost, aPlayer, aWorld, aFSM)) 
	{
		Game::GetInstance().GetGameplayManager()->OnKilledByGhost();
		return;
	}

	myTimer -= aTime->DeltaTime();

	MotionComponent* motionComp = aGhost->GetMotionComponent();
	if(motionComp->IsCenteredAtCurrentTile())
	{ 
		// check to swap to scatter
		if (myTimer <= 0)
		{
			myTimer = 0;
			aFSM->SetToState(BehaviourType::Scatter, aWorld);
			return;
		}

		// otherwise we chase the position of the player
		Coord2i playerTilepos = aPlayer->GetMotionComponent().GetTilePos();
		ChaseGhostBehaviour::ChasePlayer(myDesiredMotionDir, aPlayer, myParams->myPlayerTileOffset, aGhost, aWorld, myPath);
	}
	
	motionComp->Move(myDesiredMotionDir);
}

void ChaseGhostBehaviour::ChasePlayer(Vector2f & aCurrentDesiredMotionDir, Pacman * aPlayer, const Coord2i & offset, Ghost * ghost, World * aWorld, std::list<Coord2i>& aPath)
{
	MotionComponent* motionComp = ghost->GetMotionComponent();
	Coord2i currentTilePos = motionComp->GetTilePos();

	if (motionComp->IsCenteredAtCurrentTile())
	{
		if (!FollowPath(aCurrentDesiredMotionDir, ghost, aWorld, aPath))
		{
			const MotionComponent & playerMotionComp = aPlayer->GetMotionComponent();
			Coord2i targetPos;

			if (offset != Coord2i(0, 0))
			{
				Vector2f playerMotionDir = playerMotionComp.GetMotionDir();

				float sgnX = Utilities::Sign(static_cast<float>(offset.myX));
				float sgnY = Utilities::Sign(static_cast<float>(offset.myY));
				int nIter;
				if (offset.myX != 0)
				{
					playerMotionDir = playerMotionDir * sgnX;
					nIter = static_cast<int>(abs(offset.myX));
				}
				else
				{
					playerMotionDir = Vector2f(sgnY * playerMotionDir.myY, -sgnY * playerMotionDir.myX); // perpendicular dir
					nIter = static_cast<int>(abs(offset.myY));
				}

				targetPos = playerMotionComp.GetTilePos();
				Coord2i tileMotionDir(static_cast<int>(playerMotionDir.myX), static_cast<int>(playerMotionDir.myY));
				for (int i = 0; i < nIter; ++i)
					GetNextChaseTile(targetPos, tileMotionDir, aWorld);
			}
			else
			{
				targetPos = playerMotionComp.GetTilePos();
			}

			aPath.clear();
			PathFinding::AStar::GetPath(currentTilePos, targetPos, aPath, aWorld);
		}
	}
}

void ChaseGhostBehaviour::ChasePosition(Vector2f & aCurrentDesiredMotionDir, const Coord2i & targetPos, Ghost* ghost, World* aWorld, std::list<Coord2i>& aPath)
{
	MotionComponent* motionComp = ghost->GetMotionComponent();
	Coord2i currentTilePos = motionComp->GetTilePos();

	if (motionComp->IsCenteredAtCurrentTile())
	{
		if (!FollowPath(aCurrentDesiredMotionDir, ghost, aWorld, aPath))
		{
			aPath.clear();
			PathFinding::AStar::GetPath(currentTilePos, targetPos, aPath, aWorld);
		}
	}
}

bool ChaseGhostBehaviour::FollowPath(Vector2f & aCurrentDesiredMotionDir, Ghost * ghost, World * aWorld, std::list<Coord2i>& aPath)
{
	MotionComponent* motionComp = ghost->GetMotionComponent();
	Coord2i currentTilePos = motionComp->GetTilePos();

	if (motionComp->IsCenteredAtCurrentTile())
	{
		if (!aPath.empty())
		{
			Coord2i nextTile = aPath.front();
			aPath.pop_front();

			aCurrentDesiredMotionDir = Vector2f(Utilities::Sign(static_cast<float>(nextTile.myX - currentTilePos.myX)), Utilities::Sign(static_cast<float>(nextTile.myY - currentTilePos.myY)));
			return true;
		}
		else
			return false;
	}
	return true;
}

void ChaseGhostBehaviour::GetNextChaseTile(Coord2i & pos, Coord2i & dir, World * aWorld)
{
	Coord2i orgDir = dir;
	Coord2i nextTile = pos + dir;
	if (aWorld->IsTileWalkable(nextTile.myX, nextTile.myY))
	{
		pos = nextTile;
		return;
	}

	//try a perpendicular dir counter clock wise
	dir = Coord2i(-dir.myY, dir.myX);
	nextTile = pos + dir;
	if (aWorld->IsTileWalkable(nextTile.myX, nextTile.myY))
	{
		pos = nextTile;
		return;
	}

	//try the other direction of the perpendicular dir
	dir = Coord2i(-dir.myX, -dir.myY);
	nextTile = pos + dir;
	if (aWorld->IsTileWalkable(nextTile.myX, nextTile.myY))
	{
		pos = nextTile;
		return;
	}
	
	// if no direction is possible, then return from where you came
	dir = Coord2i(-orgDir.myX, -orgDir.myY);
	pos = pos + dir;
}

void ChaseGhostBehaviour::GetWanderDirection(Vector2f & aCurrentDesiredMotionDir, Ghost * ghost, World * aWorld, bool useMotionDir)
{
	MotionComponent* motionComp = ghost->GetMotionComponent();
	Vector2f dir = useMotionDir ? motionComp->GetMotionDir() : aCurrentDesiredMotionDir;

	Coord2i tilePos = motionComp->GetTilePos();
	Coord2i nextTilePos = tilePos;
	Coord2i motionDir = Coord2i(static_cast<int>(dir.myX), static_cast<int>(dir.myY));
	ChaseGhostBehaviour::GetNextChaseTile(nextTilePos, motionDir, aWorld);

	Coord2i dirToNext = nextTilePos - tilePos;
	aCurrentDesiredMotionDir = Vector2f(static_cast<float>(dirToNext.myX), static_cast<float>(dirToNext.myY));
}

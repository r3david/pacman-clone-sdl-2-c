//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef GHOST_H
#define GHOST_H

#include <list>
#include "Vector2f.h"
#include "Drawer.h"
#include "EnemiesDef.h"
#include "MotionComponent.h"
#include "StaticTextureRenderer.h"
#include "GhostBehaviourFSM.h"

class World;

class Ghost : public GameEntity
{
public:
	Ghost(GhostType aType);
	~Ghost();

	bool Init(EnemiesDef* aDef, World* aWorld, ResourcesManager* aResourcesManager);
	void DeInit();
	void Draw(Drawer* aDrawer);
	void Update(GameTime* aTime, Pacman* aPlayer, World* aWorld);
	void AtGameStarts(World* aWorld);

	// events
	void OnGhostKilled(World* aWorld);
	void OnVulnerable(World* aWorld);
	void OnVulnerableTimerFinishes(World* aWorld);
	void OnFinishedResting(World* aWorld);

	MotionComponent* GetMotionComponent() { return myMotionComponent; }
	StaticTextureRenderer* GetRenderer() { return myStaticTextureRenderer; }
	GhostParams* GetParams() { return myParams; }

private:
	GhostType myGhostType;
	StaticTextureRenderer* myStaticTextureRenderer;
	MotionComponent* myMotionComponent;
	GhostBehaviourFSM* myGhostBehaviourFSM;
	TextureAsset* myGhostTexture;
	TextureAsset* myGhostDeadTexture;
	TextureAsset* myGhostVulnerableTexture;
	GhostParams* myParams;
};

#endif // GHOST_H
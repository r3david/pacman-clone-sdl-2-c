//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef VECTOR2F_H
#define VECTOR2F_H

#include <math.h> 

struct Vector2f
{
public:
	Vector2f()
	{
		myX = 0.f;
		myY = 0.f;
	}

	Vector2f(float anX, float anY)
	{
		myX = anX;
		myY = anY;
	}

	const Vector2f Vector2f::operator-(const Vector2f &other) const 
	{
		Vector2f v(myX - other.myX, myY - other.myY);
		return v;
	}

	const Vector2f Vector2f::operator+(const Vector2f &other) const 
	{
		Vector2f v(myX + other.myX, myY + other.myY);
		return v;
	}

	const Vector2f Vector2f::operator*(const Vector2f& other) const 
	{
		Vector2f v(myX*other.myX, myY*other.myY);
		return v;
	}

	Vector2f& Vector2f::operator+=(const Vector2f &other) 
	{
		myX = myX + other.myX;
		myY = myY + other.myY;

		return *this;
	}

	Vector2f& Vector2f::operator*=(const float aFloat) 
	{
		myX *= aFloat;
		myY *= aFloat;

		return *this;
	}

	Vector2f& Vector2f::operator/=(const float aFloat) 
	{
		myX /= aFloat;
		myY /= aFloat;

		return *this;
	}

	const Vector2f Vector2f::operator*(const float aValue) const 
	{
		Vector2f v(myX * aValue, myY * aValue);
		return v;
	}

	bool Vector2f::operator==(const Vector2f& other) const 
	{
		return myX == other.myX && myY == other.myY;
	}

	bool Vector2f::operator!=(const Vector2f& other) const
	{
		return myX != other.myX || myY != other.myY;
	}


	float Vector2f::Length() const
	{
		return static_cast<float>(sqrt(myX*myX + myY*myY));
	}

	void Vector2f::Normalize()
	{
		float length = Length();

		if (length > 0.f)
			*this /= length;
	}

	static float Dot(const Vector2f & left, const Vector2f & right)
	{
		return left.myX * right.myX + left.myY * right.myY;
	}

	float myX;
	float myY;
};

#endif // VECTOR2F_H
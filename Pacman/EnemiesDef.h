//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef ENEMIES_DEF_H
#define ENEMIES_DEF_H

#include "Coord2i.h"
#include "Vector2f.h"
#include <vector>

enum GhostType : int
{
	Red = 0,
	Pink,
	Cyan,
	Orange
};

struct ChaseBehaviourParams
{
	Vector2f myInitialDesiredMotionDir = Vector2f(0, -1.f);
	Coord2i myPlayerTileOffset = Coord2i(0, 0);
	float mySpeed = 5;
	float myTime = 20;
};

struct ScatterBehaviourParams
{
	Coord2i myTargetTile = Coord2i(0, 0);
	float mySpeed = 5;
	float myTime = 9;
};

struct VulnerableBehaviourParams
{
	float mySpeed = 4;
	float vulnerableTime = 10;
};

struct RestBehaviourParams
{
	float myTime = 5;
};

struct GhostParams
{
	ChaseBehaviourParams myChaseParams;
	ScatterBehaviourParams myScatterParams;
	VulnerableBehaviourParams myVulnerableParams;
	RestBehaviourParams myRestParams;
	
	Vector2f mySpawnPosition = Vector2f(13.f, 13.f);
};

struct EnemiesDef
{
	GhostParams myRedGhostParams;
	GhostParams myCyanGhostParams;
	GhostParams myOrangeGhostParams;
	GhostParams myPinkGhostParams;

	static EnemiesDef CreateNewEnemiesDef()
	{
		EnemiesDef data;

		// red def
		data.myRedGhostParams.myChaseParams.myInitialDesiredMotionDir = Vector2f(0, -1.f);
		data.myRedGhostParams.myChaseParams.myPlayerTileOffset = Coord2i(0, 0);
		data.myRedGhostParams.myChaseParams.mySpeed = 4;
		data.myRedGhostParams.myChaseParams.myTime = 20;

		data.myRedGhostParams.myScatterParams.myTargetTile = Coord2i(0, 0);
		data.myRedGhostParams.myScatterParams.mySpeed = 4.f;
		data.myRedGhostParams.myScatterParams.myTime = 12;

		data.myRedGhostParams.myVulnerableParams.mySpeed = 4.f;
		data.myRedGhostParams.myRestParams.myTime = 1.5f;

		data.myRedGhostParams.mySpawnPosition = Vector2f(13.f, 13.f);

		// cyan def
		data.myCyanGhostParams.myChaseParams.myInitialDesiredMotionDir = Vector2f(0, -1.f);
		data.myCyanGhostParams.myChaseParams.myPlayerTileOffset = Coord2i(4, 0);
		data.myCyanGhostParams.myChaseParams.mySpeed = 4.f;
		data.myCyanGhostParams.myChaseParams.myTime = 15;

		data.myCyanGhostParams.myScatterParams.myTargetTile = Coord2i(25, 0);
		data.myCyanGhostParams.myScatterParams.mySpeed = 4.f;
		data.myCyanGhostParams.myScatterParams.myTime = 15;

		data.myCyanGhostParams.myVulnerableParams.mySpeed = 4.f;
		data.myCyanGhostParams.myRestParams.myTime = 3;

		data.myCyanGhostParams.mySpawnPosition = Vector2f(12.f, 13.f);

		// orange def
		data.myOrangeGhostParams.myChaseParams.myInitialDesiredMotionDir = Vector2f(0, -1.f);
		data.myOrangeGhostParams.myChaseParams.myPlayerTileOffset = Coord2i(0, 4);
		data.myOrangeGhostParams.myChaseParams.mySpeed = 4.f;
		data.myOrangeGhostParams.myChaseParams.myTime = 10;

		data.myOrangeGhostParams.myScatterParams.myTargetTile = Coord2i(25, 28);
		data.myOrangeGhostParams.myScatterParams.mySpeed = 4.f;
		data.myOrangeGhostParams.myScatterParams.myTime = 9;

		data.myOrangeGhostParams.myVulnerableParams.mySpeed = 4.f;
		data.myOrangeGhostParams.myRestParams.myTime = 4;

		data.myOrangeGhostParams.mySpawnPosition = Vector2f(11.f, 13.f);

		// pink def
		data.myPinkGhostParams.myChaseParams.myInitialDesiredMotionDir = Vector2f(0, -1.f);
		data.myPinkGhostParams.myChaseParams.myPlayerTileOffset = Coord2i(-4, 0);
		data.myPinkGhostParams.myChaseParams.mySpeed = 4.f;
		data.myPinkGhostParams.myChaseParams.myTime = 25.f;

		data.myPinkGhostParams.myScatterParams.myTargetTile = Coord2i(0, 28);
		data.myPinkGhostParams.myScatterParams.mySpeed = 3.8f;
		data.myPinkGhostParams.myScatterParams.myTime = 12.f;

		data.myPinkGhostParams.myVulnerableParams.mySpeed = 4.f;
		data.myPinkGhostParams.myRestParams.myTime = 5;

		data.myPinkGhostParams.mySpawnPosition = Vector2f(14.f, 13.f);

		return data; //I know this returns a copy 
	}

};

#endif // PACMAN_DEF_H

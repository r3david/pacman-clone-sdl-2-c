//-----------------------------------------------------------------------------
// Copyright (c) 2017 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "PathFinding.h"
#include <vector>
#include <queue>
#include <unordered_map> 
#include <functional>

namespace PathFinding
{
	void AStar::GetPath(Coord2i start, Coord2i goal, std::list<Coord2i>& aPath, World * aWorld)
	{
		std::unordered_map<size_t, Coord2i> cameFrom;
		std::unordered_map<size_t, size_t> costSoFar;
		std::priority_queue<FrontierPoint, std::vector<FrontierPoint>, std::greater<FrontierPoint>> frontier;

		// initialization
		size_t startIndex = aWorld->GetTileIndexForTileCoordinates(start.myX, start.myY);
		size_t goalIndex = aWorld->GetTileIndexForTileCoordinates(goal.myX, goal.myY);

		frontier.push({ 0, start });
		cameFrom[startIndex] = start;
		costSoFar[startIndex] = 0;

		// A* search
		static std::vector<Coord2i> neighboursOffsets = { Coord2i(1,0), Coord2i(0,1), Coord2i(-1,0), Coord2i(0,-1) };
		while (!frontier.empty()) 
		{
			FrontierPoint current = frontier.top();
			frontier.pop();

			//early exit! if we have reached the goal
			if (current.point == goal) 
				break;

			//iterate through neighbours
			for (size_t k = 0; k < neighboursOffsets.size(); ++k) 
			{
				Coord2i nPoint = current.point + neighboursOffsets[k];

				//skip points out of the image range and on where we cannot move
				if (!aWorld->IsTileWalkable(nPoint.myX, nPoint.myY)) 
					continue;

				size_t currentPointIndex = aWorld->GetTileIndexForTileCoordinates(current.point.myX, current.point.myY);
				size_t nPointIndex = aWorld->GetTileIndexForTileCoordinates(nPoint.myX, nPoint.myY);
				
				size_t newCost = costSoFar[currentPointIndex] + 1; // + graph.cost(current, next);
				if (!costSoFar.count(nPointIndex) || newCost < costSoFar[nPointIndex]) 
				{
					costSoFar[nPointIndex] = newCost;
					size_t priority = newCost + ManhattanDistance(nPoint, goal);
					frontier.push({ priority, nPoint });
					cameFrom[nPointIndex] = current.point;
				}
			}
		}

		//see if we have found the path succesfully
		if (cameFrom.find(goalIndex) == cameFrom.end())
		{
			aPath.clear();
			return;
		}

		//backtrack the path (doesnt include the start pos)
		Coord2i navCurrent = goal;
		while (navCurrent != start) 
		{
			aPath.push_front(navCurrent);
			size_t navIdx = aWorld->GetTileIndexForTileCoordinates(navCurrent.myX, navCurrent.myY);
			navCurrent = cameFrom[navIdx];
		}
	}

	size_t AStar::ManhattanDistance(const Coord2i & a, const Coord2i & b)
	{
		return abs(a.myX - b.myX) + abs(a.myY - b.myY);
	}

	void Dijkstra::GetPath(int aFromX, int aFromY, int aToX, int aToY, std::list<PathmapTile*>& aList, World* aWorld)
	{
		PathmapTile* fromTile = aWorld->GetTile(aFromX, aFromY);
		PathmapTile* toTile = aWorld->GetTile(aToX, aToY);

		std::vector<PathmapTile*>& pathmapTiles = aWorld->GetTilesArray();
		for (std::vector<PathmapTile*>::iterator list_iter = pathmapTiles.begin(); list_iter != pathmapTiles.end(); ++list_iter)
		{
			PathmapTile* tile = *list_iter;
			tile->myIsVisitedFlag = false;
		}

		Pathfind(fromTile, toTile, aList, aWorld);
	}

	bool Dijkstra::Pathfind(PathmapTile* aFromTile, PathmapTile* aToTile, std::list<PathmapTile*>& aList, World* aWorld)
	{
		aFromTile->myIsVisitedFlag = true;

		if (aFromTile->myIsBlockingFlag)
			return false;

		if (aFromTile == aToTile)
			return true;

		std::list<PathmapTile*> neighborList;

		PathmapTile* up = aWorld->GetTile(aFromTile->myX, aFromTile->myY - 1);
		if (up && !up->myIsVisitedFlag && !up->myIsBlockingFlag && ListDoesNotContain(up, aList))
			neighborList.push_front(up);

		PathmapTile* down = aWorld->GetTile(aFromTile->myX, aFromTile->myY + 1);
		if (down && !down->myIsVisitedFlag && !down->myIsBlockingFlag && ListDoesNotContain(down, aList))
			neighborList.push_front(down);

		PathmapTile* right = aWorld->GetTile(aFromTile->myX + 1, aFromTile->myY);
		if (right && !right->myIsVisitedFlag && !right->myIsBlockingFlag && ListDoesNotContain(right, aList))
			neighborList.push_front(right);

		PathmapTile* left = aWorld->GetTile(aFromTile->myX - 1, aFromTile->myY);
		if (left && !left->myIsVisitedFlag && !left->myIsBlockingFlag && ListDoesNotContain(left, aList))
			neighborList.push_front(left);

		neighborList.sort([](const PathmapTile* a, const PathmapTile* b)
		{
			int la = abs(a->myX - 13) + abs(a->myY - 13);
			int lb = abs(b->myX - 13) + abs(b->myY - 13);

			return la < lb;
		});

		for (std::list<PathmapTile*>::iterator list_iter = neighborList.begin(); list_iter != neighborList.end(); list_iter++)
		{
			PathmapTile* tile = *list_iter;

			aList.push_back(tile);

			if (Pathfind(tile, aToTile, aList, aWorld))
				return true;

			aList.pop_back();
		}

		return false;
	}

	bool Dijkstra::ListDoesNotContain(PathmapTile* aFromTile, std::list<PathmapTile*>& aList)
	{
		for (std::list<PathmapTile*>::iterator list_iter = aList.begin(); list_iter != aList.end(); list_iter++)
		{
			PathmapTile* tile = *list_iter;
			if (tile == aFromTile)
			{
				return false;
			}
		}

		return true;
	}

}